input_sentence = input("Enter the sentence, please:")

# creation of dictionary from the input sentence
dictionary_of_words = {word: len(word) for word in input_sentence.split()}
print("\n" + "------Creation of dictionary from the input sentence------")
print(dictionary_of_words)

# sorted descending values in the dictionary #dictionary_of_words
sorted_dictionary_of_words_keys = sorted(dictionary_of_words.keys(),  key=len, reverse=True)
sorted_dictionary_of_words_values = sorted(dictionary_of_words.values(), reverse=True)
sorted_dictionary_of_words = zip(sorted_dictionary_of_words_keys, sorted_dictionary_of_words_values)
print("\n" + "------Sorted descending values in the dictionary #dictionary_of_words------")
print(dict(sorted_dictionary_of_words))


# creation of list in which there are only even numbers from 0 to 10
list_even_numbers = [i for i in range(0, 10) if i % 2 == 0]
print("\n" + "------Creation of list in which there are only even numbers from 0 to 10------")
print(list_even_numbers)

# creation of list in which there are only odd numbers from 0 to 10
list_odd_numbers = [i for i in range(0, 10) if i % 2 != 0]
print("\n" + "------Creation of list in which there are only odd numbers from 0 to 10------")
print(list_odd_numbers)

# creation of dictionary from values of two lists
zip_dictionary = zip(list_even_numbers, list_odd_numbers)
print("\n" + "------Creation of dictionary from values of two lists------")
print(dict(zip_dictionary))
