def convert_fahrenheit_to_celsius(fahrenheit):
    celsius = (fahrenheit - 32) * 5/9
    print('%.2f Celsius is %.1f Fahrenheit' % (celsius, fahrenheit))


convert_fahrenheit_to_celsius(45)


def total_second(hour, minutes, seconds):
    total_amount_of_seconds = hour * 3600 + minutes * 60 + seconds
    print('Total amount of seconds: %d' % total_amount_of_seconds)


total_second(2, 10, 60)
total_second(minutes=5, seconds=10, hour=1)


def total_sum(number_1=4, number_2=3, number_3=5):
    sum_total = number_1 + number_2 + number_3
    print('Total sum of 3 numbers: %d' % sum_total)


total_sum()
total_sum(2, 6)
total_sum(1, 6, 13)

quotient = lambda x, y: x//y


print('\nQuotient: %d' % quotient(7, 2))


def circumference(radius, arg1, arg2):
    result = radius(arg1, arg2) * 2 * 3.14
    return result


print('\nCircumference: %d' % circumference(quotient, 7, 2))
