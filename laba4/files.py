file1 = open("files/file1.txt", 'w')
file1.write("У лукоморья дуб зеленый;\n"
            "Златая цепь на дубе том:\n"
            "И днем и ночью кот ученый;\n"
            "Идет направо - песнь заводит,")

file1.close()

file1 = open("files/file1.txt")

file2 = open("files/file2.txt", 'w')


def write_only_string_which_starts_on_special_symbol():
    count = 0
    for str in file1:
        if str.startswith('И'):
            file2.write(str)
            count += 1
    print("Number of strings, that were written into file2.txt: ", count)


write_only_string_which_starts_on_special_symbol()

file2.close()
file2 = open("files/file2.txt")

file1.close()
file1 = open("files/file1.txt")

strings_file1 = file1.read()
strings_file2 = file2.read()


def print_file_according_to_user_choice():
    input_str = input("Enter the name of file you want to see:")
    if input_str == 'file1':
        print(strings_file1)
    elif input_str == 'file2':
        print(strings_file2)
    else:
        print("Please, enter the right name! (Example: file1)")
        print_file_according_to_user_choice()


print_file_according_to_user_choice()

file1.close()
file2.close()
