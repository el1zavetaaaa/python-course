input_radius = input("Enter the radius, please:")


def circumference(radius):
    result = radius * radius * 3.14
    print('Площадь круга с радиусом %9.2f равна %9.3f' % (radius, result))
    string_result = 'Площадь круга с радиусом {0:5.1f} равна {1:13.4f}'.format(radius, result)
    print(string_result)


circumference(float(input_radius))


def string_format(number):
    string_format = '\nРадиус в восьмеричном представлении {0:^6o} и в шестнадцатеричном представлении {0:^10x} ,\n' \
                    'в експоненциальном представлении {0:E}, в десятичном виде {0:4d}'.format(34)
    print(string_format)


string_format(34)


